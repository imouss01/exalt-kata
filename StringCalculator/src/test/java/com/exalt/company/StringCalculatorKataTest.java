package com.exalt.company;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.stream.Stream;

import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.exalt.company.exception.InvalidFirstLineException;
import com.exalt.company.exception.InvalidSeparatorException;
import com.exalt.company.exception.NegativeNumberException;

public class StringCalculatorKataTest {

	@Test
	void add_should_throw_NullPointerException_when_input_is_null() {
		StringCalculatorKata stringCalculatorKata = new StringCalculatorKata();

		ThrowableAssert.ThrowingCallable callable = () -> stringCalculatorKata.add(null);

		assertThatThrownBy(callable).isInstanceOf(NullPointerException.class).hasMessage("The input can not be null.");
	}

	private static Stream<Arguments> notSupportedSeparator() {
		return Stream.of(Arguments.of(new StringCalculatorKata(), "1@2"),
				Arguments.of(new StringCalculatorKata(";"), "10,20"));
	}
	
	@ParameterizedTest
	@MethodSource("notSupportedSeparator")
	void add_should_throw_InvalidSeparatorException_when_input_separator_is_not_supported(
			StringCalculatorKata stringCalculatorKata, String input) {

		ThrowableAssert.ThrowingCallable callable = () -> stringCalculatorKata.add(input);

		assertThatThrownBy(callable).isInstanceOf(InvalidSeparatorException.class)
				.hasMessage(String.format("The separator of numbers is invalid. It should be [%s] or [\n].", stringCalculatorKata.getSeparator()));
	}

	@Test
	void add_should_throw_IllegalArgumentException_when_input_size_is_more_than_max_characters() {
		StringCalculatorKata stringCalculatorKata = new StringCalculatorKata();

		ThrowableAssert.ThrowingCallable callable = () -> stringCalculatorKata.add("1,2,3");

		assertThatThrownBy(callable).isInstanceOf(IllegalArgumentException.class)
				.hasMessage("The max characters should be 2");
	}

	@Test
	void add_should_throw_InvalidFirstLineException_when_first_line_is_provided_and_not_contains_supported_separator() {
		StringCalculatorKata stringCalculatorKata = new StringCalculatorKata(",");

		ThrowableAssert.ThrowingCallable callable = () -> stringCalculatorKata.add("//\n1,2,3");

		assertThatThrownBy(callable).isInstanceOf(InvalidFirstLineException.class)
				.hasMessage("The first line should contains a supported separator.");
	}

	@Test
	void add_should_throw_NegativeNumberException_when_a_negative_number_is_provided() {
		StringCalculatorKata stringCalculatorKata = new StringCalculatorKata(",");

		ThrowableAssert.ThrowingCallable callable = () -> stringCalculatorKata.add("1,-2,-3");

		assertThatThrownBy(callable).isInstanceOf(NegativeNumberException.class)
				.hasMessage("The negative numbers [-2, -3] are not allowed. The number should be positive");
	}

	@Test
	void add_should_throw_NumberFormatException_when_input_contains_non_number_character() {
		StringCalculatorKata stringCalculatorKata = new StringCalculatorKata(",");

		ThrowableAssert.ThrowingCallable callable = () -> stringCalculatorKata.add("a,1,2,3, b");

		assertThatThrownBy(callable).isInstanceOf(NumberFormatException.class);
	}

	private static Stream<Arguments> successOperations() {
		return Stream.of(
				Arguments.of("Empty input scenario", new StringCalculatorKata(), "", 0),
				Arguments.of("White space input scenario", new StringCalculatorKata(), " ", 0),
				Arguments.of("2 numbers maximum separated by comma scenario", new StringCalculatorKata(), "10,20", 30),
				Arguments.of("Unknown numbers scenario", new StringCalculatorKata(","), "10,20,30", 60),
				Arguments.of("New lines between numbers scenario", new StringCalculatorKata(","), "10\n20,30,40",100),
				Arguments.of("New lines between numbers scenario", new StringCalculatorKata(","), "10\n20,30\n40",100),
				Arguments.of("Change separator scenario", new StringCalculatorKata(";"), "//;\n10;20;30;40;50",150),
				Arguments.of("Change first line format scenario", new StringCalculatorKata(";"), "----;\n10;20;30;40;50",150)
			);
	}
	
	@ParameterizedTest(name = "{0}")
	@MethodSource("successOperations")
	void add_should_return_result(String scenarioName, StringCalculatorKata stringCalculatorKata, String input,
			int expected) throws InvalidSeparatorException, InvalidFirstLineException, NegativeNumberException {
		int result = stringCalculatorKata.add(input);

		assertThat(expected).isEqualTo(result);
	}
}
