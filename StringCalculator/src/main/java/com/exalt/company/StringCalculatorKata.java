package com.exalt.company;

import static org.junit.jupiter.api.DynamicTest.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.exalt.company.exception.InvalidFirstLineException;
import com.exalt.company.exception.InvalidSeparatorException;
import com.exalt.company.exception.NegativeNumberException;

public class StringCalculatorKata {

	private final int maxCharacters;
	private final String separator;

	private static final String lineReturn = "\n";

	public StringCalculatorKata() {
		maxCharacters = 2;
		separator = ",";
	}

	public StringCalculatorKata(String separator) {
		maxCharacters = Integer.MAX_VALUE;
		this.separator = separator;
	}

	int add(String numbers) throws InvalidSeparatorException, InvalidFirstLineException, NegativeNumberException {
		Objects.requireNonNull(numbers, "The input can not be null.");
		
		if (numbers.isBlank()) {
			return 0;
		}

		if (!numbers.contains(separator) && !numbers.contains(lineReturn)) {
			throw new InvalidSeparatorException(String.format("The separator of numbers is invalid. It should be [%s] or [\n].", separator));
		}

		if (numbers.split(separator).length > maxCharacters) {
			throw new IllegalArgumentException("The max characters should be " + maxCharacters);

		}

		return computeSum(numbers);
	}

	private int computeSum(String numbers) throws InvalidFirstLineException, InvalidSeparatorException, NegativeNumberException {

		String[] elements = numbers.split(lineReturn);

		String firstLine = elements[0];
		boolean isNumber = firstLine.matches("\\d+");

		if (!isNumber && !firstLine.contains(separator)) {
			throw new InvalidFirstLineException("The first line should contains a supported separator.");
		}

		List<String> stringCharacters = Arrays.stream(elements).flatMap(element -> Stream.of(element.split(separator))).collect(Collectors.toList());

		if (!isNumber && firstLine.contains(separator) && elements.length > 1) {
			stringCharacters.remove(0);
		}

		List<Integer> values = stringCharacters.stream().map(Integer::parseInt).collect(Collectors.toUnmodifiableList());

		int sum = 0;
		for (int value : values) {
			if (value < 0) {
				List<Integer> negativeNumbers = values.stream().filter(number -> number < 0).collect(Collectors.toUnmodifiableList());
				throw new NegativeNumberException(String.format("The negative numbers %s are not allowed. The number should be positive", negativeNumbers));
			}
			
			sum += value;
		}

		return sum;
	}
	
	public final String getSeparator() {
		return separator;
	}
}
