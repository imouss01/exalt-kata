package com.exalt.company.exception;

public class InvalidFirstLineException extends Exception {

    public InvalidFirstLineException(String message) {
        super(message);
    }
}
