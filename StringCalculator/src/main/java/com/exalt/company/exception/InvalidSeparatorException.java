package com.exalt.company.exception;

public class InvalidSeparatorException extends Exception {

    public InvalidSeparatorException(String message) {
        super(message);
    }
}
